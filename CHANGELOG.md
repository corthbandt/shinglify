# v0.4.0

- Window move to next screen

# v0.3.0

- Active Tilers
- Smarter XFCE GenMon integration
- Better performance
- Window creation/deletion/movement events

# v0.2.0

- Better window placement precision
- Various detail improvements

# v0.1.0

- Initial public version
