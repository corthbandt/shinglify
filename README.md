# Shinglify

* [Intro](#tldr)
* [Installation](#install)
* [Features](#functionality)
* [Limitations](#limitations)
* [Configuration](#config)
* [Reference](#luaref)
* [Default Configuration](#exconf)
* [Why???](#why)

## TLDR; 

<a name="tldr"></a>
Shinglify aims to emulate a tiling window manager within your regular desktop environment to give you:

* Automatic window tiling
* Efficient keyboard navigation
* Virtual Workspaces that work 

These three major features are fully orthogonal. You can use just one, two or all three to your liking.

Shinglify would not be possible without the awesome work of github.com/BurntSushi (for the X11 stuff) and github.com/yuin/gopher-lua (for Lua). 

## Installation

<a name="install"></a>
Download the executable from https://gitlab.com/corthbandt/shinglify-bin/-/tree/latest or `go build` it. Make it auto-start after login.

## Default Configuration

* Meta-Shift-Left/Right/Up/Down - Spatial window navigation
* Meta-Control-Left/Right/Up/Down - Move window to adjacent screen 
* Meta-Prior - Activate the previous window across all screens
* Meta-Next - Activate the next window across all screens
* Meta-Shift-Prior - Activate the previous window on the same screen
* Meta-Shift-Next - Activate the next window on the same screens
* Meta-Control-Prior - Activate the previous window on the same tag
* Meta-Control-Next - Activate the next window on the same tag
* Meta-Space - Maximize/Minimize the active window
* Meta-1/2/3/4/5/6/7/8/9 - Switch to tags 1-5
* Meta-Shift-1/2/3/4/5/6/7/8/9 - Send active window to tag 1-5
* Meta-Shift-a - Set current screen to grid tiling
* Meta-Shift-s - Set current screen to master/stack tiling
* Meta-Shift-d - Set current screen to tabbed tiling
* Meta-Shift-f - Disable tiling on the current screen

## What Shinglify Does

<a name="functionality"></a>

Shinglify adds WM-like workspaces, tiling and window navigation to traditional desktop environments.

Currently, it mainly targets XFCE. There are provisions for multiple backends, for now the only one is X11/EWMH. This means it technically works in other environments too. It is also tested against Gnome, Cinnamon, KDE Plasma, Mate and various other environments. Support heavily depends on the respective window manager implementing X11/EWMH in a reasonable way.

Wayland unfortunately currently does not work. At this time, the Wayland ecosystem lacks the means for window manipulation (I'm aware of wlr-foreign-toplevel-management). The wayland developers consider this largely a not-my-problem and I guess they will want to figure out copy'n'paste first anyway.

## Limitations

<a name="limitations"></a>

### Wayland

Wayland currently is not supported. Later, if they get their act together.

### Gnome

Gnome technically supports EWMH, but it is doing weird things with window geometry. There is a Gnome-specific tweak in place but it is not perfect. Proper support would require using DBus. With Gnome habitually breaking APIs whenever they feel like, this seems wasteful unless there is actual demand.

### Minimum Window Size

Shinglify currently does not respect any kind of minimum size for windows. This may break the tiling.

### Static Key Bindings

Key bindings are only applied on the initial run of the config script.

### No Dynamic Config

To change the configuration, you have to stop and restart.

### XFCE GenMon

* The GenMon integration does not have any configuration yet.
* To avoid the GenMon display getting too long, the text is culled in a very simple way.
* Clicking does not do anything yet.
* Screen layout changes are detected, but panel config usually does not follow.

## Configuration

<a name="config"></a>

Shinglify is configured through a single Lua script. This script is running when Shinglify starts and whenever something relevant happens to your windows. This allows custom behaviours if you need them.

The possible config script locations are currently hard-coded. This list is tried in order:

- $HOME/.config/shinglify/shinglify.lua
- $HOME/.config/shinglify.lua
- $HOME/.shinglify.lua
- /etc/shinglify.lua
- builtin

A default config is built into the executable and can be retrieved by running `shinglify --print-default-config`.

### Keyboard Focus

All functionality is triggered via keyboard bindings.

Example:
```lua
key_bind("Mod4-shift-left",navigate_left)
```

This causes Mod4-Shift-Left to change the active window to the one left of the currently active window.

The second argument `navigate_left` is a Lua function that gets executed when the keyboard combination is pressed.

Example:
```lua
key_bind("Mod4-1",function() show_tag("1") end)
```

Here the key combination triggers an inline lua function.

### Window Navigation

Shinglify provides ten built-in navigation functions:

| Function                  | Behaviour                                                      |
| ------------------------- | -------------------------------------------------------------- |
| `navigate_left`           | Switch to the closest window left of the currently active one  | 
| `navigate_right`          | Switch to the closest window right of the currently active one |
| `navigate_up`             | Switch to the closest window above the currently active one    |
| `navigate_down`           | Switch to the closest window below the currently active one    |
| `navigate_next`           | Switch to the next window across all screens                   |
| `navigate_prev`           | Switch to the previous window across all screens               |
| `navigate_next_on_screen` | Switch to the next window on the same screen                   |
| `navigate_prev_on_screen` | Switch to the previous window on the same screen               |
| `navigate_next_on_tag`    | Switch to the next window on the same tag                      |
| `navigate_prev_on_tag`    | Switch to the previous window on the same tag                  |

The left/right/up/down navigation functions employ a number of heuristics to determine what is "closest", considering window overlapping, previous activations and relative layout.

### Workspaces/Tags

Workspaces in Shinglify are called "Tags", primarily to avoid confusion with DE-native workspaces. Shinglify Tags are largely _incompatible_ with regular workspaces provided by the desktop environment. Use one or the other, but not both.

The tag names do not have to be numbers and the names have no relation to the keyboard bindings.

### Window Tiling

Shinglify provides for per-screen automatic tiling with optional gaps. Tiling is performed by tiler functions exposed to lua or written in lua.

### XFCE Status Display

Shinglify has special support for XFCE. It provides for tiling status display through one or more panels that have a "Generic Monitor" item.

The assumed setup is to have one panel per screen, with one Shinglify-assigned GenMon item.

The GenMon item has to be set up in a specific way to be detected:

Command: "cat FILENAME"
Label: "shinglify" or "shinglify-SCREENID"

The filename for the command should be temporary and unique. `/tmp/shinglify_SCREENID` would work nicely.

If the label specifies a SCREENID then this item will show the Shinglify status for that screen.

If the label does not specify a SCREENID, it is auto-detected from the panels' position.

## Lua Function Reference

<a name="luaref"></a>

The configuration script is running completely on every single event.

### `evt_type():string`

Returns the type of the event currently being handled.

This is one of:

* ScreenChange - when the screen layout changes
* WindowChange - when windows activate, open or close
* Quit - when shinglify quits
* KeyPress - when a registered keycombo is pressed
* KeyRelease - when a registered keycombo is released
* Error - when an error occurs
* Raw - a raw backend event, mainly for debugging

### `evt_data():string`

Returns additional event data, notable an error message for "Error" events and the keycombo for "KeyPress"/"KeyRelease" events. 

### `evt_param():any`

Returns optional, variant event parameters.

### `key_bind(keycombo,keypressfun[,keyreleasefun])`

Register a key combination.

### `get_wm_state():window_manager_state`

Get a structure describing the full state of the window manager.

### `get_active_windowid():string`

Shortcut to get the active window ID out of the current window manager state.

### `get_active_screenid():string`

Shortcut to get the active screen ID out of the current window manager state.

### `get_window_screenid(windowid):string`

Shortcut to get the screen ID out for a window. 

### `navigate_left()`

Activate the next visible window left of the currently active one.

### `navigate_right()`

Activate the next visible window right of the currently active one.

### `navigate_up()`

Activate the next visible window above the currently active one.

### `navigate_down()`

Activate the next visible window below the currently active one.

### `navigate_next()`

Activate the next window in the global window list.

### `navigate_prev()`

Activate the previous window in the global window list.

### `navigate_next_on_screen()`

Activate the next window on the same screen.

### `navigate_prev_on_screen()`

Activate the previous window on the same screen.

### `navigate_next_on_tag()`

Activate the next window with the same tag.

### `navigate_prev_on_tag()`

Activate the previous window with the same tag.

### `navigate_next_tag_on_screen()`

Activate the next tag on the currently active screen.

### `navigate_prev_tag_on_screen()`

Activate the previous tag on the currently active screen.

### `move_window_left()`

Move the active window to the next screen left.

### `move_window_right()`

Move the active window to the next screen right.

### `move_window_up()`

Move the active window to the next screen up.

### `move_window_down()`

Move the active window to the next screen down.

### `apply_tags()`

Update visiblity according to tag assignments.

### `register_tags(tag[,tag...])`

Register tag names.

### `set_active_window_tag(tag)`

Set the tag on the active window.

### `set_window_tag(windowid,tag)`

Set the tag on any window. Setting an empty tag clears the window tag.

### `add_screen_tag(screenid,tag)`

Set a tag to show on a specific screen.

### `show_tag(tag)`

Make windows with given tag visible.

### `tag_visible(tag):bool`

Check whether a given tag is currently visible.

### `show_window(windowid,show_or_hide)`

Show (true) or hide (false) a window.

### `maximize_window(windowid,switch)`

Minimize or maximize a window.

Switch:

* 0 - restore/unmaximize
* 1 - maximize
* 2 - toggle

### `run_tiler(screenid,windowid,tilerfun)`

Apply the current tiling rules. All three parameters may be nil, causing the active window/screen and/or preset tiler being used.

### `tiler_helper_get_screen_layout_params(screenid,single):usable_rect,innergap,dx,dy`

Helper function for building scripted tilers. Returns the usable rectangle and the inter-window gap for a given screen.
The return values ```dx``` and ```dy``` will allow tiling size adjustments in later versions

### `set_tiler([screenid][,tilerfun][,automatic])`

Set the tiler for the screen identified by `screenid` or the default tiler if `screenid` is ommitted.
If ```automatic``` is true, the set tiler is re-run automatically when windows open or close.

### `get_tiler([screenid]):tilerfun`

Get the tiler for `screenid` or the default one if `screenid` is ommitted.

### `enable_tiler(screenid,enable)`

Enable or disable the tiler on a screen without changing its settings.

### `set_gaps(screenid,innergap,outergap,singlegap)`

When screenid is "", the global default gaps are set. If screenid is not empty, only the gaps for that screen are set.

### `tiler_none:tilerfun`

tiler_none is a no-op tiling function.

### `tiler_grid:tilerfun`

tiler_grid arranges all windows in a grid.

### `tiler_masterstack:tilerfun`

tiler_masterstack applies a master-stack tiling configuration.

### `tiler_vtabbed:tilerfun`

tiler_vtabbed arranges windows as vertical tabs.

### `tiler_vtabbed_stagger:tilerfun`

tiler_vtabbed_stagger arranges windows as vertical tabs with a slight horizontal stagger.

### `enableui_xfcegenmon(with_icon:bool,active_pango,inactive_pango)`

enableui_xfcegenmon activates the special integration with xfce and the genmon panel module.


## Annotated Default Configuration

<a name="exconf"></a>

```lua
-- enable the XFCE GenMon integration. See the README for details.

if (evt_type()=="Init") then

    enableui_xfcegenmon(true,"bgcolor='#a0d0b0' fgcolor='#000000' ","fgcolor='#c0c0c0'")

    screen_layout=get_screen_layout()

    if(screen_layout=="DP1-1.VX3276-QHD.V9W201241091.2560.0.2560.1440;DP1-2.VX3276-QHD.V9W201241100.0.0.2560.1440;HDMI1.DELL U2415.7MT0187H1JVU.5120.240.1920.1200")then
        move_xfcepanel_to_screen(1,"DP1-1")
        move_xfcepanel_to_screen(5,"DP1-2")
        move_xfcepanel_to_screen(6,"HDMI1")

        add_screen_tag("DP1-1","1","2","3")
        add_screen_tag("DP1-2","4","5")
        add_screen_tag("HDMI1","6","7")
    end


    if(screen_layout=="DP1-1.DELL P2314H.J8J3144NBC1S.3840.0.1920.1080;DP1-2.DELL P2314H.J8J3144DEFTS.1920.0.1920.1080;eDP1.2401.0.0.393.1920.1200")then
        move_xfcepanel_to_screen(6,"DP1-1")
        move_xfcepanel_to_screen(1,"DP1-2")
        move_xfcepanel_to_screen(5,"eDP1")

        add_screen_tag("DP1-2","1","2","3")
        add_screen_tag("DP1-1","4","5")
        add_screen_tag("eDP1","6","7")
    end

end

-- Set the default tiler. This does not actually do much, but these are the options for tilers.

--set_tiler(tiler_grid)
--set_tiler(tiler_masterstack)
--set_tiler(tiler_vtabbed)
--set_tiler(tiler_vtabbed_stagger)


-- Set the gaps globally. Inner, outer, single
-- Inner is the gap between windows when tiled
-- Outer is the gap between windows and the border of the usable screen space (considering panels)
-- Single is the gap around a single window on a tiled screen 
set_gaps("",5,5,5)

-- Set up navigation keys
-- navigate spatially among visible (non-hidden, not obstructed) windows
key_bind("Mod4-shift-left",navigate_left)
key_bind("Mod4-shift-right",navigate_right)
key_bind("Mod4-shift-up",navigate_up)
key_bind("Mod4-shift-down",navigate_down)

-- navigate to next/prev window globally
key_bind("Mod4-prior",navigate_prev)
key_bind("Mod4-next",navigate_next)

-- navigate between screens
key_bind("Mod4-shift-prior",navigate_prev_on_screen)
key_bind("Mod4-shift-next",navigate_next_on_screen)

-- navigate between tags on the same screen
key_bind("Mod4-control-prior",navigate_prev_tag_on_screen)
key_bind("Mod4-control-next",navigate_next_tag_on_screen)

-- move active window to next screen
key_bind("Mod4-control-left",move_window_left)
key_bind("Mod4-control-right",move_window_right)
key_bind("Mod4-control-up",move_window_up)
key_bind("Mod4-control-down",move_window_down)

-- Set a key combo to toggle maximized/normal window size

key_bind("Mod4-space",function() maximize_window(get_active_windowid(),2) end)

-- Set key combos for tags. Tags can be any text and there can be any number.


key_bind("Mod4-shift-1",function() set_active_window_tag("1") end)
key_bind("Mod4-shift-2",function() set_active_window_tag("2") end)
key_bind("Mod4-shift-3",function() set_active_window_tag("3") end)
key_bind("Mod4-shift-4",function() set_active_window_tag("4") end)
key_bind("Mod4-shift-5",function() set_active_window_tag("5") end)
key_bind("Mod4-shift-6",function() set_active_window_tag("6") end)
key_bind("Mod4-shift-7",function() set_active_window_tag("7") end)
key_bind("Mod4-shift-8",function() set_active_window_tag("8") end)
key_bind("Mod4-shift-9",function() set_active_window_tag("9") end)
key_bind("Mod4-shift-0",function() set_active_window_tag("") end)
key_bind("Mod4-1",function() show_tag("1") end)
key_bind("Mod4-2",function() show_tag("2") end)
key_bind("Mod4-3",function() show_tag("3") end)
key_bind("Mod4-4",function() show_tag("4") end)
key_bind("Mod4-5",function() show_tag("5") end)
key_bind("Mod4-6",function() show_tag("6") end)
key_bind("Mod4-7",function() show_tag("7") end)
key_bind("Mod4-8",function() show_tag("8") end)
key_bind("Mod4-9",function() show_tag("9") end)

-- Set key combos for tiling

key_bind("Mod4-shift-a",function()set_tiler(get_active_screenid(),tiler_grid,true)end)
key_bind("Mod4-shift-s",function()set_tiler(get_active_screenid(),tiler_masterstack,true)end)
key_bind("Mod4-shift-d",function()set_tiler(get_active_screenid(),tiler_vtabbed,true)end)
key_bind("Mod4-shift-f",function()set_tiler(get_active_screenid(),nil,false)end)
```


## Why???

<a name="why"></a>
The Why of Shinglify is obviously highly subjective. Take it with a grain of salt. Or two.

### Tiling Window Managers 

Tiling window managers are arguably more efficient for keyboard-centric workflows, with quicker arrangement of windows, keyboard-driven window navigation and semantic grouping of windows.

What tiling window managers generally lack is user friendliness. I consider this a problem. Having nice a GUI to configure your audio, networking or bluetooth is important to me. Because I cannot be bothered to learn and remember the terminal invocations to connect to a WiFi. Why should I if I do this maybe ten times a year? My brain capacity is better wasted elsewhere.

Tiling window managers also often lack in usability (example: notifications) and discoverability.

Yes, things like notifications can be added, but this requires considerable effort on the part of the user. There's a reason people show off their "ricing" on reddit. Because it is soooo much work to get right.

### Desktop Environments

Full-blown desktop environments on the other hand provide all the bells and whistles for tasks you'd rather not spend half a day googling. Yay for that!

What DEs (Gnome, KDE, XFCE, MATE, Cinnamon, ...) lack is consistent and efficient keyboard-driven workflows.

Workspaces are just a mess across the board. If they work at all, they fail at multi-monitor setups.

Keyboard-driven navigation requires digging through configuration dialogs and often third-party extensions. And then they go and change whether your workspaces go up/down or left/right for no good reason, breaking everything.

### Conclusion

Tiling WMs fall short in user friendliness. DEs lack in efficiency. Choose your poison.

Or... don't!

