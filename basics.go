package shinglify

// Max returns the larger of a and b
func Max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

// Min returns the smaller of a and b
func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// Overlap returns the absolute overlap of the two ranges (a0,a1) and (b0,b1) or <=0 if no overlap. For no overlap, the value gives the distance
func Overlap(a0, a1, b0, b1 int) int {
	if a1 < b0 {
		return a1 - b0
	}
	if a0 > b1 {
		return b1 - a0
	}
	return Min(a1, b1) - Max(a0, b0)
}
