package main

import (
	"runtime"

	"gitlab.com/corthbandt/shinglify"
)

func main() {
	runtime.GOMAXPROCS(1)
	shinglify.Main()
}
