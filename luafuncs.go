package shinglify

import (
	"fmt"
	"os/exec"
	"sort"
	"strings"
	"time"

	"git.pixeltamer.net/gopub/logfunk"
	lua "github.com/yuin/gopher-lua"
)

func (inst *Inst) LuaKeyBind(L *lua.LState) int {
	key := L.ToString(1)
	arg2 := L.Get(2)
	arg3 := L.Get(3)
	if key == "" {
		L.Error(lua.LString("invalid bind value '"+key+"'"), 0)
		return 0
	}
	var evtDataT1 lua.LValue
	var evtDataT2 lua.LValue
	if arg2.Type() != lua.LTNil {
		evtDataT1 = arg2
	}
	if arg3.Type() != lua.LTNil {
		evtDataT2 = arg3
	}

	if inst.EvtType != "Init" {
		return 0
	}
	err := inst.WMBE.RegisterKeyBind(key, evtDataT1, evtDataT2)
	if err != nil {
		inst.Log("Error binding %v: %v", key, err.Error())
	}
	return 0
}

func (inst *Inst) LuaLog(L *lua.LState) int {
	l := L.GetTop()
	t := []string{}
	for i := 1; i <= l; i++ {
		s := L.Get(i).String()
		if s != "" {
			t = append(t, s)
		}
	}
	inst.Log("%v", strings.Join(t, " "))
	return 0
}

func (inst *Inst) LuaEvtType(L *lua.LState) int {
	L.Push(lua.LString(inst.EvtType))
	return 1
}

func (inst *Inst) LuaEvtData(L *lua.LState) int {
	L.Push(lua.LString(inst.EvtData))
	return 1
}

func (inst *Inst) LuaEvtParam(L *lua.LState) int {
	if inst.EvtParam != nil {
		L.Push(inst.EvtParam)
	} else {
		L.Push(lua.LNil)
	}
	return 1
}

func (inst *Inst) LuaRofiWindowSwitcher(L *lua.LState) int {
	inst.RofiWindowSwitcher()
	return 0
}

func (inst *Inst) LuaPauseUpdates(L *lua.LState) int {
	inst.pauseUpdates++
	return 0
}

func (inst *Inst) LuaUnpauseUpdates(L *lua.LState) int {
	inst.pauseUpdates--
	if inst.pauseUpdates == 0 {
		inst.ApplyTagVisiblity(true, "")
	}
	return 0
}

func (inst *Inst) LuaGetScreenLayout(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	L.Push(lua.LString(wms.ScreenLayouts))
	return 1
}

func (inst *Inst) LuaMatchScreenLayout(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	match := true
	args := []string{}
	l := L.GetTop()
	for i := 1; i <= l; i++ {
		s := L.Get(i).String()
		if s != "" {
			args = append(args, s)
		}
	}
	for _, scr := range wms.Screens {
		found := false
	argloop:
		for iarg, arg := range args {
			if arg == scr.ID || arg == scr.Serial || arg == scr.Product {
				found = true
				args = append(args[:iarg], args[iarg+1:]...)
				break argloop
			}
		}
		if !found {
			match = false
		}
	}
	if len(args) == 0 && match {
		L.Push(lua.LTrue)
	} else {
		L.Push(lua.LFalse)
	}
	return 1
}

func (inst *Inst) LuaMoveXFCEPanelToScreen(L *lua.LState) int {
	panelid := L.CheckString(1)
	screenid := L.CheckString(2)
	if panelid == "" {
		return 0
	}
	if screenid == "" {
		inst.Log("Hiding panel %v", panelid)
		exec.Command("xfconf-query", "-c", "xfce4-panel", "-np", "/panels/panel-"+panelid+"/autohide-behavior", "-t", "int", "-s", "2").Run()
	} else {
		inst.Log("Showing panel %v on %v", panelid, screenid)
		exec.Command("xfconf-query", "-c", "xfce4-panel", "-np", "/panels/panel-"+panelid+"/autohide-behavior", "-t", "int", "-s", "0").Run()
	}
	wms := inst.WMBE.State(false)
	if screenid == "" {
		//		exec.Command("xfconf-query", "-c", "xfce4-panel", "-np", "/panels/panel-"+panelid+"/position", "-t", "string", "-s", fmt.Sprintf("p=1;x=%v;y=%v", -16384, -16384)).Run()
		exec.Command("xfconf-query", "-c", "xfce4-panel", "-np", "/panels/panel-"+panelid+"/position", "-t", "string", "-s", fmt.Sprintf("p=1;x=%v;y=%v", 0, 0)).Run()
	} else {
		screen := wms.GetScreen(screenid)
		if screen == nil {
			inst.Log(logfunk.Error, "Screen '%v' not found", screenid)
			return 0
		}
		exec.Command("xfconf-query", "-c", "xfce4-panel", "-np", "/panels/panel-"+panelid+"/position", "-t", "string", "-s", fmt.Sprintf("p=8;x=%v;y=%v", screen.Geometry.X, screen.Geometry.Y)).Run()
	}
	wms = inst.WMBE.State(false)
	if inst.pauseUpdates == 0 {
		for _, uibe := range inst.UIBackends {
			uibe.ScreenUpdate(wms, []Evt{{Timestamp: time.Now(), Type: EvtTypeScreenChange}})
		}
	}
	return 0
}

func (inst *Inst) LuaGetWMState(L *lua.LState) int {
	refresh := L.OptBool(1, false)
	wms := inst.WMBE.State(refresh)
	stt := L.NewTable()
	{
		stt.RawSet(lua.LString("WMName"), lua.LString(wms.WMName))
		actwnd := wms.GetActiveWindowID()
		stt.RawSet(lua.LString("ActiveWindowID"), lua.LString(actwnd))
		stt.RawSet(lua.LString("ScreenLayouts"), lua.LString(wms.ScreenLayouts))
		stt.RawSet(lua.LString("ScreensConnected"), lua.LString(wms.ScreensConnected))
	}
	{
		lst := L.NewTable()
		for _, wndid := range wms.ActiveWindowHistory {
			lst.Append(lua.LString(wndid))
		}
		stt.RawSet(lua.LString("WindowActivity"), lst)
	}
	{
		tm := make(map[string]struct{})
		for _, w := range wms.Windows {
			if w.Tag != "" {
				tm[w.Tag] = struct{}{}
			}
		}
		for _, s := range wms.Screens {
			for _, t := range s.Tags {
				tm[t] = struct{}{}
			}
		}
		tl := []string{}
		for k := range tm {
			tl = append(tl, k)
		}
		sort.Strings(tl)
		lst := L.NewTable()
		for _, tag := range tl {
			lst.Append(lua.LString(tag))
		}
		stt.RawSet(lua.LString("Tags"), lst)
	}
	{
		lst := L.NewTable()
		for _, wnd := range wms.Windows {
			lwnd := L.NewTable()
			lwnd.RawSet(lua.LString("ID"), lua.LString(wnd.ID))
			lwnd.RawSet(lua.LString("Title"), lua.LString(wnd.Title))
			lwnd.RawSet(lua.LString("Class"), lua.LString(wnd.Class))
			lwnd.RawSet(lua.LString("Instance"), lua.LString(wnd.Instance))
			tlst := L.NewTable()
			for _, t := range wnd.Types {
				tlst.RawSet(lua.LString(t), lua.LTrue)
			}
			lwnd.RawSet(lua.LString("Types"), tlst)
			lwnd.RawSet(lua.LString("Desktop"), lua.LString(wnd.Desktop))
			lwnd.RawSet(lua.LString("X"), lua.LNumber(wnd.Geometry.X))
			lwnd.RawSet(lua.LString("Y"), lua.LNumber(wnd.Geometry.Y))
			lwnd.RawSet(lua.LString("W"), lua.LNumber(wnd.Geometry.W))
			lwnd.RawSet(lua.LString("H"), lua.LNumber(wnd.Geometry.H))
			lwnd.RawSet(lua.LString("Active"), lua.LBool(wnd.Active))
			lwnd.RawSet(lua.LString("Visible"), lua.LBool(wnd.Visible))
			lwnd.RawSet(lua.LString("Special"), lua.LBool(wnd.Special))
			lwnd.RawSet(lua.LString("Panel"), lua.LBool(wnd.Panel))
			lwnd.RawSet(lua.LString("PrimaryScreen"), lua.LString(wnd.PrimaryScreen))
			lwnd.RawSet(lua.LString("SingleScreen"), lua.LBool(wnd.SingleScreen))
			lwnd.RawSet(lua.LString("Tag"), lua.LString(wnd.Tag))

			lst.Append(lwnd)
		}
		stt.RawSet(lua.LString("Windows"), lst)
	}
	{
		lst := L.NewTable()
		idlst := L.NewTable()
		for scri, scr := range wms.Screens {
			lscr := L.NewTable()
			lscr.RawSet(lua.LString("ID"), lua.LString(scr.ID))
			lscr.RawSet(lua.LString("Index"), lua.LNumber(scri))
			lscr.RawSet(lua.LString("Name"), lua.LString(scr.Name))
			lscr.RawSet(lua.LString("X"), lua.LNumber(scr.Geometry.X))
			lscr.RawSet(lua.LString("Y"), lua.LNumber(scr.Geometry.Y))
			lscr.RawSet(lua.LString("W"), lua.LNumber(scr.Geometry.W))
			lscr.RawSet(lua.LString("H"), lua.LNumber(scr.Geometry.H))
			lscr.RawSet(lua.LString("Manufacturer"), lua.LString(scr.Manufacturer))
			lscr.RawSet(lua.LString("Product"), lua.LString(scr.Product))
			lscr.RawSet(lua.LString("Serial"), lua.LString(scr.Serial))
			lscr.RawSet(lua.LString("ActiveTag"), lua.LString(scr.ActiveTag))
			tl := L.NewTable()
			for _, t := range scr.Tags {
				tl.Append(lua.LString(t))
			}
			lscr.RawSet(lua.LString("TagList"), tl)
			lst.RawSet(lua.LString(scr.ID), lscr)
			idlst.Append(lua.LString(scr.ID))
		}
		stt.RawSet(lua.LString("Screens"), lst)
		stt.RawSet(lua.LString("ScreenIDs"), idlst)
	}
	{
		lst := L.NewTable()
		idlst := L.NewTable()
		for dski, dsk := range wms.Desktops {
			lscr := L.NewTable()
			lscr.RawSet(lua.LString("ID"), lua.LString(dsk.ID))
			lscr.RawSet(lua.LString("Index"), lua.LNumber(dski))
			lscr.RawSet(lua.LString("Name"), lua.LString(dsk.Name))
			lscr.RawSet(lua.LString("X"), lua.LNumber(dsk.Geometry.X))
			lscr.RawSet(lua.LString("Y"), lua.LNumber(dsk.Geometry.Y))
			lscr.RawSet(lua.LString("W"), lua.LNumber(dsk.Geometry.W))
			lscr.RawSet(lua.LString("H"), lua.LNumber(dsk.Geometry.H))
			lscr.RawSet(lua.LString("Active"), lua.LBool(dsk.Active))
			lst.RawSet(lua.LString(dsk.ID), lscr)
			idlst.Append(lua.LString(dsk.ID))
		}
		stt.RawSet(lua.LString("Desktops"), lst)
		stt.RawSet(lua.LString("DesktopIDs"), idlst)
	}
	L.Push(stt)
	return 1
}

func (inst *Inst) LuaGetWindowScreenID(L *lua.LState) int {
	wndid := L.CheckString(1)
	if wndid == "" {
		return 0
	}
	wms := inst.WMBE.State(false)
	wnd := wms.GetWindow(wndid)
	if wnd == nil {
		return 0
	}
	L.Push(lua.LString(wnd.PrimaryScreen))
	return 1
}

func (inst *Inst) LuaGetActiveWindowID(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	L.Push(lua.LString(wms.GetActiveWindowID()))
	return 1
}

func (inst *Inst) LuaGetActiveScreenID(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wnd := wms.GetWindow(wms.GetActiveWindowID())
	if wnd != nil {
		L.Push(lua.LString(wnd.PrimaryScreen))
	} else {
		L.Push(lua.LString(""))
	}
	return 1
}

func (inst *Inst) LuaRepositionWindow(L *lua.LState) int {
	wndid := L.CheckString(1)
	wms := inst.WMBE.State(true)
	wnd := wms.GetWindow(wndid)
	if wnd != nil {
		scr := wms.GetScreen(wnd.PrimaryScreen)
		if scr != nil {
			inst.WMBE.Move(wndid, wnd.Geometry)
		}
	}
	return 0
}

func (inst *Inst) LuaShowWindow(L *lua.LState) int {
	wndid := L.CheckString(1)
	show := L.CheckBool(2)
	if wndid == "" {
		return 0
	}
	inst.WMBE.Show(wndid, show)
	return 0
}

func (inst *Inst) LuaMaximizeWindow(L *lua.LState) int {
	wndid := L.CheckString(1)
	dir := L.CheckInt(2)
	if wndid == "" {
		return 0
	}
	inst.WMBE.Maximize(wndid, dir)
	return 0
}

func (inst *Inst) LuaEnableUIXFCEGenMon(L *lua.LState) int {
	if inst.EvtType != "Init" {
		return 0
	}
	for _, be := range inst.UIBackends {
		if _, ok := be.(*XFCEGenMonUI); ok {
			return 0
		}
	}
	wmName := inst.WMBE.State(false).WMName
	if !strings.Contains(strings.ToLower(wmName), "xfwm") {
		inst.Log("Not enabling XFCEGenMon due to WM name being %v", wmName)
		return 0
	}
	be := &XFCEGenMonUI{}
	be.showIcon = L.OptBool(1, true)

	be.styleActive = L.OptString(2, "default")
	be.styleInactive = L.OptString(3, "default")
	be.styleInactiveFront = L.OptString(4, "default")

	if be.styleActive == "default" {
		be.styleActive = "bgcolor='#a0d0b0' fgcolor='#000000'"
		be.styleActive = "bgcolor='#a0d0b0' fgcolor='#000000'"
	}
	if be.styleInactive == "default" {
		be.styleInactive = "fgcolor='#c0c0c0'"
		be.styleInactive = "fgcolor='#aaaaaa'"
	}
	if be.styleInactiveFront == "default" {
		be.styleInactiveFront = "fgcolor='#dddddd'"
		be.styleInactiveFront = "bgcolor='#777777' fgcolor='#ffffff'"
	}
	be.showWindows = L.OptBool(5, true)
	be.showIcon = L.OptBool(6, true)
	err := be.Init(inst)
	if err != nil {
		L.Error(lua.LString(err.Error()), 0)
		return 0
	}
	inst.UIBackends = append(inst.UIBackends, be)
	return 0
}
