package shinglify

import "fmt"

func (inst *Inst) PrintOutputInfo() {

	wms := inst.WMBE.State(false)
	_ = wms
	fmt.Printf("Outputs:\n")
	for _, o := range wms.Outputs {
		if o.Geometry.IsZero() {
			fmt.Printf("ID: %-8v  Manufacturer: %-3v Product: %-13v Serial: %-13v Mapping: off\n", o.ID, o.Manufacturer, o.Product, o.Serial)
		} else {
			fmt.Printf("ID: %-8v  Manufacturer: %-3v Product: %-13v Serial: %-13v Mapping: %vx%v+%v+%v2\n", o.ID, o.Manufacturer, o.Product, o.Serial, o.Geometry.W, o.Geometry.H, o.Geometry.X, o.Geometry.Y)
		}
	}
}
