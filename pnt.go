package shinglify

// Pnt encodes a 2D integer position
type Pnt struct {
	X, Y int
}
