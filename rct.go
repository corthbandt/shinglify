package shinglify

import "fmt"

// Rct encodes a 2D integer rectangle as top-left coordinates X/Y, width and height W/H. The right and bottom columns/rows (X+W, Y+H) are not considered part of the area.
type Rct struct {
	X, Y, W, H int
}

func (r Rct) R() int {
	return r.X + r.W
}

func (r Rct) B() int {
	return r.Y + r.H
}

func (r Rct) TopLeft() Pnt {
	return Pnt{X: r.X, Y: r.Y}
}

func (r Rct) TopRight() Pnt {
	return Pnt{X: r.X + r.W, Y: r.Y}
}

func (r Rct) BottomLeft() Pnt {
	return Pnt{X: r.X, Y: r.Y + r.H}
}

func (r Rct) BottomRight() Pnt {
	return Pnt{X: r.X + r.W, Y: r.Y + r.H}
}

func (r Rct) Area() int {
	return r.W * r.H
}

func (r Rct) Valid() bool {
	return r.W > 0 && r.H > 0
}

func (r Rct) IsZero() bool {
	return r.W == 0 && r.H == 0 && r.X == 0 && r.Y == 0
}

func (r Rct) Hit(p Pnt) bool {
	return (p.X >= r.X) && (p.Y >= r.Y) && (p.X < r.X+r.W) && (p.Y < r.Y+r.H)
}

func (r Rct) Intersect(r2 Rct) bool {
	return (r.X < r2.X+r2.W) && (r.X+r.W > r2.X) && (r.Y < r2.Y+r2.H) && (r.Y+r.H > r2.Y)
}

func (r Rct) Intersection(r2 Rct) Rct {
	x0 := r.X
	if r.X <= r2.X {
		x0 = r2.X
	}
	y0 := r.Y
	if r.Y <= r2.Y {
		y0 = r2.Y
	}
	x1 := r2.X + r2.W
	if r.X+r.W < r2.X+r2.W {
		x1 = r.X + r.W
	}
	y1 := r2.Y + r2.H
	if r.Y+r.H < r2.Y+r2.H {
		y1 = r.Y + r.H
	}
	return Rct{X: x0, Y: y0, W: x1 - x0, H: y1 - y0}
}

func (r Rct) String() string {
	return fmt.Sprintf("%4v,%4v/%4v,%4v/%4v,%4v", r.X, r.Y, r.W, r.H, r.X+r.W, r.Y+r.H)
}

func MkRct(x, y, w, h int) Rct {
	return Rct{X: x, Y: y, W: w, H: h}
}
