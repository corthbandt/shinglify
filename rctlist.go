package shinglify

// RctList is a list of disjunct (i.e. non-overlapping) rectangles (Rct)
type RctList struct {
	Rcts []Rct
}

func (rl *RctList) Len() int {
	return len(rl.Rcts)
}

func (rl *RctList) Clear() {
	rl.Rcts = nil
}

// Hit returns true if p is within any of the rectangles
func (rl *RctList) Hit(p Pnt) bool {
	for _, r := range rl.Rcts {
		if r.Hit(p) {
			return true
		}
	}
	return false
}

func (rl *RctList) Bounds() Rct {
	if len(rl.Rcts) == 0 {
		return Rct{}
	}
	x0 := rl.Rcts[0].X
	x1 := x0 + rl.Rcts[0].W
	y0 := rl.Rcts[0].Y
	y1 := y0 + rl.Rcts[0].H
	for _, sr := range rl.Rcts {
		x0 = Min(x0, sr.X)
		y0 = Min(y0, sr.Y)
		x1 = Max(x1, sr.X+sr.W)
		y1 = Max(y1, sr.Y+sr.H)
	}
	return Rct{X: x0, Y: y0, W: x1 - x0, H: y1 - y0}
}

func (rl *RctList) Area() int {
	a := 0
	for _, r := range rl.Rcts {
		a += r.W * r.H
	}
	return a
}

// Add r to the combined area of the list. This may change large parts of the list to ensure the non-overlapping property.
func (rl *RctList) Add(r Rct) {
	if !r.Valid() {
		return
	}
	if len(rl.Rcts) == 0 {
		rl.Rcts = append(rl.Rcts, r)
		return
	}
	lrtb := func(l, r, t, b int) Rct {
		return Rct{X: l, Y: t, W: r - l, H: b - t}
	}
	todo := []Rct{r}
	for len(todo) > 0 {
		cur := todo[len(todo)-1]
		todo = todo[:len(todo)-1]
		addIt := true
		curLen := len(rl.Rcts)
		if !cur.Valid() {
			continue
		}
	innerloop:
		for i := 0; i < curLen; i++ {
			ir := rl.Rcts[i]
			if ir.Intersect(cur) {
				addIt = false
				if (cur.X < ir.X) || (cur.X+cur.W > ir.X+ir.W) || (cur.Y < ir.Y) || (cur.Y+cur.H > ir.Y+ir.H) {
					t := Max(cur.Y, ir.Y)
					b := Min(cur.Y+cur.H, ir.Y+ir.H)
					todo = append(todo,
						lrtb(cur.X, cur.X+cur.W, cur.Y, ir.Y),
						lrtb(cur.X, cur.X+cur.W, ir.Y+ir.H, cur.Y+cur.H),
						lrtb(cur.X, ir.X, t, b),
						lrtb(ir.X, cur.X, t, b))
					break innerloop
				}
			}
		}
		if addIt {
			rl.Rcts = append(rl.Rcts, cur)
		}
	}
}

// Remove r from the combined area of the list. This may change large parts of the list to ensure the non-overlapping property.
func (rl *RctList) Sub(r Rct) {
	todo := []Rct{}
	for i := 0; i < len(rl.Rcts); i++ {
		c := rl.Rcts[i]
		if !r.Intersect(c) {
			todo = append(todo, c)
		} else {
			t := Max(c.Y, r.Y)
			b := Min(c.Y+c.H, r.Y+r.H)
			todo = append(todo,
				Rct{c.X, c.Y, c.W, r.Y - c.Y},
				Rct{c.X, r.Y + r.H, c.W, c.Y + c.H - (r.Y + r.H)},
				Rct{c.X, t, r.X - c.X, b - t},
				Rct{r.X + r.W, t, c.X + c.W - (r.X + r.W), b - t})
		}
	}

	rl.Rcts = []Rct{}
	for _, tr := range todo {
		if tr.Valid() {
			rl.Rcts = append(rl.Rcts, tr)
		}
	}
}

func (rl *RctList) Compact() {
}
