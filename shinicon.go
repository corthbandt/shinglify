package shinglify

import (
	"image"
	"image/color"
)

func (inst *Inst) GetShinIcon(size int) image.Image {
	inst.ShinIconCacheLock.Lock()
	defer inst.ShinIconCacheLock.Unlock()
	if i, ok := inst.ShinIconCache[size]; ok {
		return i
	}
	i := image.NewRGBA(image.Rect(0, 0, size, size))
	bgcolor := color.RGBA{R: 255, G: 255, B: 255, A: 255}
	fgcolor := color.RGBA{R: 0, G: 0, B: 80, A: 255}
	t1 := size / 5
	t2 := size - t1
	for y := 0; y < size; y++ {
		for x := 0; x < size; x++ {
			if x >= t1 && x < t2 && y >= t1 && y < t2 {
				i.Set(x, y, fgcolor)
			} else {
				i.Set(x, y, bgcolor)
			}
		}
	}
	inst.ShinIconCache[size] = i
	return i
}
