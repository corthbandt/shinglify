package shinglify

import (
	lua "github.com/yuin/gopher-lua"
)

func (inst *Inst) ApplyTagVisiblity(refresh bool, screenID ScreenID) {
	wms := inst.WMBE.State(refresh)

	screens := ""
	for _, scr := range wms.Screens {
		screens += scr.ID + ":"
		for _, t := range scr.Tags {
			if t == scr.ActiveTag {
				screens += "*" + t
			} else {
				screens += t
			}
			screens += ","
		}
		screens += " "
	}
	inst.Log("ApplyTagVisiblity %v %v %v", refresh, screenID, screens)
	var makeActive WindowID
	for _, wnd := range wms.Windows {
		if !wnd.Panel && !wnd.Special && !wnd.IsDesktop {
			if wnd.Tag != "" {
				if screenID == "" || screenID == wnd.PrimaryScreen {
					scr := wms.GetScreen(wnd.PrimaryScreen)
					if scr != nil && scr.ActiveTag != "" {
						if scr.ActiveTag == wnd.Tag {
							if !wnd.Visible {
								inst.Log("Tag ShowWin %v", wms.WindowInfo(wnd.ID))
								inst.WMBE.Show(wnd.ID, true)
							} else {
								inst.Log("Tag ShowWinK %v", wms.WindowInfo(wnd.ID))
							}
							makeActive = wnd.ID
						} else {
							if wnd.Visible {
								inst.Log("Tag HideWin %v", wms.WindowInfo(wnd.ID))
								inst.WMBE.Show(wnd.ID, false)
							} else {
								inst.Log("Tag HideWinK %v", wms.WindowInfo(wnd.ID))
							}
						}
					}
				}
			}
		}
	}
	if makeActive != "" {
		actWnd := wms.GetWindow(wms.GetActiveWindowID())
		if actWnd != nil && !actWnd.Visible {
			inst.Log("Tag WinActivate %v", wms.WindowInfo(makeActive))
			inst.WMBE.Activate(makeActive)
		}
	}
}

func (inst *Inst) LuaApplyTags(L *lua.LState) int {
	inst.ApplyTagVisiblity(true, "")
	return 0
}

func (inst *Inst) SetWindowTag(wndid WindowID, tag string) {
	wms := inst.WMBE.State(false)
	wnd := wms.GetWindow(wndid)
	windowIsActive := wndid == wms.GetActiveWindowID()
	if wnd != nil {
		if tag == "" {
			inst.WMBE.SetWindowTag(wndid, tag)
			inst.WMBE.State(false)

			tagscr := wms.GetScreenForTag(tag)
			if tagscr != "" {
				inst.WMBE.SetScreenTag(tagscr, tag)
				inst.ApplyTagVisiblity(false, "")
			}
			return
		}
		oldTag := wnd.Tag
		oldScrID := wnd.PrimaryScreen
		tagscr := wms.GetScreenForTag(tag)
		if tagscr == "" {
			inst.WMBE.SetScreenTag(wnd.PrimaryScreen, tag)
			tagscr = wnd.PrimaryScreen
		} else if tagscr != wnd.PrimaryScreen {
			scr := wms.GetScreen(tagscr)
			r := wnd.Geometry
			r.X = scr.Geometry.X
			r.Y = scr.Geometry.Y
			inst.WMBE.Move(wndid, r)
			if wnd.Maximized {
				inst.WMBE.Maximize(wndid, 1)
			}
			sp := inst.GetScreenParams(scr.ID, false)
			if sp.TilerAutomatic && sp.Tiler != nil {
				inst.CallTiler(scr, nil, sp.Tiler, "")
			}
		}
		if tagscr != "" {
			inst.WMBE.SetScreenTag(tagscr, tag)
		}

		inst.WMBE.SetWindowTag(wndid, tag)
		inst.WMBE.State(false)
		inst.ApplyTagVisiblity(true, "")
		// if we moved the last window away from the current screen/tag _and_ we stay on the same screen, auto-switch this screen to the new tag
		wms = inst.WMBE.State(false)
		for _, scr := range wms.Screens {
			if scr.ID == oldScrID {
				if scr.ActiveTag == oldTag {
					winPresent := false
					for _, w := range wms.Windows {
						if w.Visible && w.Tag == oldTag && !w.Panel && !w.Special && !w.IsDesktop {
							winPresent = true
						}
					}
					if !winPresent {
						inst.WMBE.SetScreenTag(oldScrID, tag)
						inst.ApplyTagVisiblity(true, oldScrID)
						if windowIsActive {
							inst.WMBE.Activate(wndid)
						}
					}
				}
			}
		}
	}
}

func (inst *Inst) LuaSetActiveWindowTag(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	tag := L.CheckString(1)
	wndid := wms.GetActiveWindowID()
	inst.Log("LuaSetActiveWindowTag %v %v", tag, wms.WindowInfo(wndid))
	inst.SetWindowTag(wndid, tag)
	return 0
}

func (inst *Inst) LuaSetWindowTag(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	wndid := L.CheckString(1)
	tag := L.CheckString(2)
	inst.Log("LuaSetWindowTag %v %v", tag, wms.WindowInfo(wndid))
	inst.SetWindowTag(wndid, tag)
	return 0
}

func (inst *Inst) LuaAddScreenTag(L *lua.LState) int {
	wms := inst.WMBE.State(false)
	scrid := L.CheckString(1)
	l := L.GetTop()
	for i := 2; i <= l; i++ {
		tag := L.CheckString(i)
		oldscrid := wms.GetScreenForTag(tag)
		if oldscrid != "" {
			inst.WMBE.RemScreenTag(oldscrid, tag)
		}
		inst.WMBE.AddScreenTag(scrid, tag)
	}
	inst.ApplyTagVisiblity(false, scrid)
	return 0
}

/*
func (inst *Inst) LuaReleaseEmptyScreenTags(L *lua.LState) int {
	usedtagsm := make(map[string]struct{})
	wms := inst.WMBE.State(true)
	for _, wnd := range wms.Windows {
		if wnd.Tag != "" {
			usedtagsm[wnd.Tag] = struct{}{}
		}
	}
	for _, scr := range wms.Screens {
		for _,t:=range scr.Tags {
			if t!=scr.ActiveTag {
				if _,ok:=usedtagsm[t];!ok {
					inst.WMBE.RemoveTag
				}
			}
		}
	}
	return 0
}
*/

func (inst *Inst) LuaShowTag(L *lua.LState) int {
	tag := L.CheckString(1)
	wms := inst.WMBE.State(false)
	found := false
	for _, scr := range wms.Screens {
		for _, t := range scr.Tags {
			if t == tag {
				inst.WMBE.SetScreenTag(scr.ID, tag)
				found = true
			}
		}
	}
	if !found {
		actwnd := wms.GetWindow(wms.GetActiveWindowID())
		if actwnd != nil {
			inst.WMBE.SetScreenTag(actwnd.PrimaryScreen, tag)
		}
	}
	inst.ApplyTagVisiblity(false, "")
	{
		activate := ""
		wms = inst.WMBE.State(true)
		actwnd := wms.GetWindow(wms.GetActiveWindowID())
		if actwnd.Tag != tag {
			for _, wnd := range wms.Windows {
				if wnd.Tag == tag && !wnd.Panel && !wnd.Special && !wnd.IsDesktop && wnd.Visible {
					activate = wnd.ID
				}
			}
		}
		if activate != "" {
			inst.WMBE.Activate(activate)
		}
	}
	return 0
}

func (inst *Inst) LuaTagVisible(L *lua.LState) int {
	tag := L.CheckString(1)
	wms := inst.WMBE.State(false)
	found := false
	for _, scr := range wms.Screens {
		if scr.ActiveTag == tag {
			found = true
		}
	}
	L.Push(lua.LBool(found))
	return 1
}
