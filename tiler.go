package shinglify

import (
	"fmt"
	"sort"
	"strings"

	lua "github.com/yuin/gopher-lua"
)

func (inst *Inst) RegisterTilers() {
	sf := func(f func(L *lua.LState) int, name string) {
		lfnm := "tiler_" + strings.ToLower(name)
		lf := inst.L.NewFunction(f)
		inst.L.SetGlobal(lfnm, lf)
		inst.TilerNames[lf] = name
	}
	sf(inst.LuaTilerNone, "None")
	sf(inst.LuaTilerGrid, "Grid")
	sf(inst.LuaTilerMasterStack, "MasterStack")
	sf(inst.LuaTilerVTabbed, "VTabbed")
	sf(inst.LuaTilerVTabbedStagger, "VTabbedStagger")

}

func (inst *Inst) CallTiler(scr *WMScreenState, wnd *WMWindowState, tilerfunc *lua.LFunction, resizeDir string) error {
	wms := inst.WMBE.State(false)

	inst.Log("CallTiler %v %v %v", scr, wnd, tilerfunc)

	if scr == nil && wnd == nil {
		actWndID := wms.GetActiveWindowID()
		wnd = wms.GetWindow(actWndID)
		scr = wms.GetScreen(wnd.PrimaryScreen)
	}

	if scr == nil && wnd != nil {
		scr = wms.GetScreen(wnd.PrimaryScreen)
	}
	if wnd == nil && scr != nil {
		for idx, w := range wms.Windows {
			if w.PrimaryScreen == scr.ID && w.Visible && !w.Panel && !w.Special {
				wnd = &wms.Windows[idx]
			}
		}
	}
	if wnd == nil || scr == nil {
		return fmt.Errorf("Can't find window/screen")
	}

	ScrParams := inst.GetScreenParams(scr.ID, false)

	if tilerfunc == nil {
		if ScrParams.Tiler != nil {
			tilerfunc = ScrParams.Tiler
		} else {
			tilerfunc = inst.DefaultScreenParams.Tiler
		}
	}

	if tilerfunc == nil {
		return fmt.Errorf("No tiler set")
	}

	wndlst := inst.GetTilerWindowIDList(scr, wnd)

	inst.Log("RunTiler %v(%v) D:%v W:%v WL:%v", scr.ID, scr.Geometry, resizeDir, wnd.Title, wndlst)
	inst.L.Push(tilerfunc)
	inst.L.Push(lua.LString(wnd.ID))
	inst.L.Push(lua.LString(scr.ID))
	lWndLst := inst.L.NewTable()
	for _, wid := range wndlst {
		lWndLst.Append(lua.LString(wid))
	}
	inst.L.Push(lWndLst)
	inst.L.Push(lua.LString(resizeDir))

	inst.L.Call(4, 1)

	resLst := []TileResult{}
	resMap := make(map[WindowID]TileResult)

	resultObj := inst.L.Get(-1)
	if resultTbl, ok := resultObj.(*lua.LTable); ok {
		len := resultTbl.Len()
		for i := 1; i <= len; i++ {
			le := resultTbl.RawGetInt(i)
			if etbl, ok := le.(*lua.LTable); ok {
				e := TileResult{}
				e.ID = etbl.RawGet(lua.LString("ID")).String()
				e.Geo.X = int(lua.LVAsNumber(etbl.RawGet(lua.LString("X"))))
				e.Geo.Y = int(lua.LVAsNumber(etbl.RawGet(lua.LString("Y"))))
				e.Geo.W = int(lua.LVAsNumber(etbl.RawGet(lua.LString("W"))))
				e.Geo.H = int(lua.LVAsNumber(etbl.RawGet(lua.LString("H"))))
				resLst = append(resLst, e)
				resMap[e.ID] = e
			}
		}
	}

	inst.L.Pop(1)

	allwndlst := []WindowID{}
	// keep all windows in the same order that are somewhere else
	for _, w := range wms.Windows {
		if _, ok := resMap[w.ID]; !ok {
			allwndlst = append(allwndlst, w.ID)
		}
	}
	for _, tr := range resLst {
		if tr.ID != wnd.ID {
			allwndlst = append(allwndlst, tr.ID)
		}
	}
	allwndlst = append(allwndlst, wnd.ID)

	inst.Log("Tile result:")
	for _, tr := range resLst {
		w := wms.GetWindow(tr.ID)
		inst.Log(" %v  OldG:%v\n", tr, w.Geometry)
	}

	for _, tr := range resLst {
		inst.WMBE.Move(tr.ID, tr.Geo)
	}
	for _, tr := range resLst {
		inst.WMBE.Move(tr.ID, tr.Geo)
	}
	for _, tr := range resLst {
		inst.WMBE.Move(tr.ID, tr.Geo)
	}
	inst.WMBE.ArrangeWindows(allwndlst)
	//	fmt.Printf("Call result: %v\n", resLst)

	return nil
}

/*
tiler functions take as parameters:
- windowid to activate
- screenid to tile
- list of windowids to arrange
tiler functions return:
- list of {ID,X,Y,W,H} tupels, last one on top, only listed stay visible
*/

type TileResult struct {
	ID  WindowID
	Geo Rct
}

// LuaRunTiler run_tiler(screenid,actwndid,tiler)
func (inst *Inst) LuaRunTiler(L *lua.LState) int {
	parActWnd := L.Get(1)
	parScr := L.Get(2)
	parTiler := L.Get(3)
	var scr *WMScreenState
	var wnd *WMWindowState
	var ftile *lua.LFunction
	wms := inst.WMBE.State(false)
	if parScr.Type() == lua.LTString {
		scr = wms.GetScreen(parScr.String())
	}
	if parActWnd.Type() == lua.LTString {
		wnd = wms.GetWindow(parActWnd.String())
	}
	if parTiler.Type() == lua.LTFunction {
		var ok bool
		ftile, ok = parTiler.(*lua.LFunction)
		if !ok {
			ftile = nil
		}
	}
	inst.CallTiler(scr, wnd, ftile, "")
	return 0
}

func (inst *Inst) LuaResizeTiler(L *lua.LState) int {
	dir := L.CheckString(1)
	if dir == "" {
		return 0
	}
	wms := inst.WMBE.State(false)
	actWndID := wms.GetActiveWindowID()
	actWnd := wms.GetWindow(actWndID)
	if actWnd == nil {
		return 0
	}
	inst.CallTiler(nil, actWnd, nil, dir)
	return 0
}

func (inst *Inst) LuaEnableTiler(L *lua.LState) int {
	screenID := L.CheckString(1)
	enable := L.CheckBool(2)
	inst.GetScreenParams(screenID, true).TilerEnable = enable
	return 0
}

func (inst *Inst) LuaSetTiler(L *lua.LState) int {
	var screenID ScreenID
	var tiler *lua.LFunction
	setActive := false
	for i := 1; i <= 3; i++ {
		if L.GetTop() >= i {
			lv := L.Get(i)
			if v, ok := lv.(lua.LString); ok {
				screenID = v.String()
			}
			if v, ok := lv.(*lua.LFunction); ok {
				tiler = v
			}
			if v, ok := lv.(lua.LBool); ok {
				setActive = bool(v)
			}
		}
	}
	if screenID == "" {
		inst.DefaultScreenParams.Tiler = tiler
	} else {
		sp := inst.GetScreenParams(screenID, true)
		sp.TilerAutomatic = setActive
		sp.Tiler = tiler
		sp.TilerEnable = true
		wms := inst.WMBE.State(false)
		scr := wms.GetScreen(screenID)
		if scr != nil {
			inst.CallTiler(scr, nil, tiler, "")
		}
	}
	return 0
}

func (inst *Inst) LuaGetTiler(L *lua.LState) int {
	scridv := L.Get(1)
	if v, ok := scridv.(lua.LString); ok {
		sp := inst.GetScreenParams(v.String(), false)
		L.Push(sp.Tiler)
		return 1
	}
	L.Push(inst.DefaultScreenParams.Tiler)
	return 1
}

func (inst *Inst) LuaSetGaps(L *lua.LState) int {
	screenID := L.CheckString(1)
	inner := L.CheckInt(2)
	outer := L.CheckInt(3)
	single := L.CheckInt(4)
	if screenID == "" {
		inst.DefaultScreenParams.GapInner = inner
		inst.DefaultScreenParams.GapOuter = outer
		inst.DefaultScreenParams.GapSingle = single
	} else {
		sp := inst.GetScreenParams(screenID, true)
		sp.GapInner = inner
		sp.GapOuter = outer
		sp.GapSingle = single
	}
	return 0
}

func (inst *Inst) LuaTilerHelperGetScreenLayoutParams(L *lua.LState) int {
	scrid := L.CheckString(1)
	single := L.CheckBool(2)
	rct, gap, tdx, tdy := inst.TilerHelperGetScreenLayoutParams(scrid, single)
	t := L.NewTable()
	t.RawSetString("X", lua.LNumber(rct.X))
	t.RawSetString("Y", lua.LNumber(rct.Y))
	t.RawSetString("W", lua.LNumber(rct.W))
	t.RawSetString("H", lua.LNumber(rct.H))
	L.Push(t)
	L.Push(lua.LNumber(gap))
	L.Push(lua.LNumber(tdx))
	L.Push(lua.LNumber(tdy))
	return 4
}

func (inst *Inst) LuaHelperParseWindowIDList(L *lua.LState, paramIdx int) WMWindowIDList {
	v := L.Get(paramIdx)
	var vl *lua.LTable
	var ok bool
	if vl, ok = v.(*lua.LTable); !ok {
		return nil
	}
	widl := WMWindowIDList{}
	wms := inst.WMBE.State(false)
	vl.ForEach(func(k, id lua.LValue) {
		ids := id.String()
		if wms.GetWindow(ids) != nil {
			widl = append(widl, ids)
		}
	})
	return widl
}

func (inst *Inst) LuaHelperTileResultList(L *lua.LState, trl []TileResult) *lua.LTable {
	tbl := L.NewTable()
	for _, tr := range trl {
		e := L.NewTable()
		e.RawSet(lua.LString("ID"), lua.LString(tr.ID))
		e.RawSet(lua.LString("X"), lua.LNumber(tr.Geo.X))
		e.RawSet(lua.LString("Y"), lua.LNumber(tr.Geo.Y))
		e.RawSet(lua.LString("W"), lua.LNumber(tr.Geo.W))
		e.RawSet(lua.LString("H"), lua.LNumber(tr.Geo.H))
		tbl.Append(e)
	}
	return tbl
}

func (inst *Inst) TilerHelperGetScreenLayoutParams(scrid ScreenID, single bool) (usable Rct, gap int, dx, dy int) {
	wms := inst.WMBE.State(false)
	scr := wms.GetScreen(scrid)
	if scr == nil {
		return Rct{X: 0, Y: 0, W: 100, H: 100}, 0, 0, 0
	}
	scrGeo := scr.Geometry
	rct := scrGeo
	l := rct.X
	t := rct.Y
	r := rct.X + rct.W
	b := rct.Y + rct.H
	for _, wnd := range wms.Windows {
		if wnd.IsDesktop && wnd.PrimaryScreen == scrid {
			if l < wnd.Geometry.X {
				l = wnd.Geometry.X
			}
			if t < wnd.Geometry.Y {
				t = wnd.Geometry.Y
			}
			if r > wnd.Geometry.X+wnd.Geometry.W {
				r = wnd.Geometry.X + wnd.Geometry.W
			}
			if b > wnd.Geometry.Y+wnd.Geometry.H {
				b = wnd.Geometry.Y + wnd.Geometry.H
			}
		}
		if wnd.Visible && wnd.Panel && wnd.PrimaryScreen == scrid {
			if wnd.Geometry.W*wnd.Geometry.H*4 < scrGeo.W*scrGeo.H {
				switch {
				case (wnd.Geometry.Y < scrGeo.Y+20) && (wnd.Geometry.H < scrGeo.H/8): // top panel
					if t < wnd.Geometry.Y+wnd.Geometry.H {
						t = wnd.Geometry.Y + wnd.Geometry.H
					}
				case (wnd.Geometry.Y+wnd.Geometry.H > scrGeo.Y+scrGeo.H-20) && (wnd.Geometry.H < scrGeo.H/8): // bottom panel
					if b > wnd.Geometry.Y {
						b = wnd.Geometry.Y
					}
				case (wnd.Geometry.X < scrGeo.X+20) && (wnd.Geometry.W < scrGeo.W/8): // left panel
					if l < wnd.Geometry.X+wnd.Geometry.W {
						l = wnd.Geometry.X + wnd.Geometry.W
					}
				case (wnd.Geometry.X+wnd.Geometry.W > scrGeo.X+scrGeo.W-20) && (wnd.Geometry.W < scrGeo.W/8): // right panel
					if r > wnd.Geometry.X {
						r = wnd.Geometry.X
					}
				}
			}
		}
	}
	// apply outer gaps
	screenParams := inst.GetScreenParams(scrid, false)

	if single {
		l += screenParams.GapSingle
		t += screenParams.GapSingle
		r -= screenParams.GapSingle
		b -= screenParams.GapSingle
	} else {
		l += screenParams.GapOuter
		t += screenParams.GapOuter
		r -= screenParams.GapOuter
		b -= screenParams.GapOuter
	}

	tdx := screenParams.TilerDX
	tdy := screenParams.TilerDY

	if inst.GnomeShellHack {
		if t < 32 {
			t = 32
		}
	}

	rct.X = l
	rct.Y = t
	rct.W = r - l
	rct.H = b - t

	return rct, screenParams.GapInner, tdx, tdy
}

func (inst *Inst) LuaTilerNone(L *lua.LState) int {
	if L.GetTop() == 0 {
		L.Push(lua.LString("None"))
		L.Push(lua.LString("N"))
		return 2
	}
	wndid := L.CheckString(1)
	scrid := L.CheckString(2)
	wndlst := inst.LuaHelperParseWindowIDList(L, 3)

	wms := inst.WMBE.State(false)
	wnd := wms.GetWindow(wndid)
	scr := wms.GetScreen(scrid)
	if wnd == nil {
		L.Error(lua.LString("invalid window id"), 0)
		return 0
	}
	if scr == nil {
		L.Error(lua.LString("invalid screen id"), 0)
		return 0
	}
	if wndlst == nil {
		L.Error(lua.LString("invalid window list id"), 0)
		return 0
	}
	_ = wnd
	_ = scr

	//	L.Push(inst.LuaHelperTileResultList(L, []TileResult{}))
	trl := []TileResult{}
	for _, w := range wndlst {
		wnd := wms.GetWindow(w)
		trl = append(trl, TileResult{ID: w, Geo: Rct{X: wnd.Geometry.X, Y: wnd.Geometry.Y, W: wnd.Geometry.W, H: wnd.Geometry.H}})
	}

	L.Push(inst.LuaHelperTileResultList(L, trl))

	return 1
}

func (inst *Inst) LuaTilerGrid(L *lua.LState) int {
	if L.GetTop() == 0 {
		L.Push(lua.LString("Grid"))
		L.Push(lua.LString("G"))
		return 2
	}
	wndid := L.CheckString(1)
	scrid := L.CheckString(2)
	wndlst := inst.LuaHelperParseWindowIDList(L, 3)

	wms := inst.WMBE.State(false)
	wnd := wms.GetWindow(wndid)
	scr := wms.GetScreen(scrid)
	if wnd == nil {
		L.Error(lua.LString("invalid window id"), 0)
		return 0
	}
	if scr == nil {
		L.Error(lua.LString("invalid screen id"), 0)
		return 0
	}
	if wndlst == nil {
		L.Error(lua.LString("invalid window list id"), 0)
		return 0
	}

	scrRct, gaps, tdx, tdy := inst.TilerHelperGetScreenLayoutParams(scrid, len(wndlst) == 1)

	tdx = 0
	tdy = 0

	cols := 0
	rows := 0
	targetcells := len(wndlst)
	bestdiff := targetcells * targetcells * 10010 * 20
	for trows := 1; trows <= targetcells; trows++ {
		for tcols := 1; tcols <= targetcells; tcols++ {
			total := trows * tcols
			if total >= targetcells {
				basediff := (total - targetcells) * 10
				rcdiff := 100
				if tcols > trows {
					rcdiff = tcols * 100 / trows
				}
				if tcols < trows {
					rcdiff = trows * 133 / tcols
				}
				diff := (basediff + 20) * rcdiff
				if diff < bestdiff {
					bestdiff = diff
					cols = tcols
					rows = trows
				}

			}
		}
	}

	trl := []TileResult{}
	usew := (scrRct.W - tdx) / cols
	useh := (scrRct.H - tdy) / rows
	ofsx := scrRct.X
	ofsy := scrRct.Y
	facx := (scrRct.W - tdx) / cols
	facy := (scrRct.H - tdy) / rows

	if cols > 1 {
		usew = ((scrRct.W - tdx) - gaps*(cols-1)) / cols
		facx = usew + gaps
	}
	if rows > 1 {
		useh = ((scrRct.H - tdy) - gaps*(rows-1)) / rows
		facy = useh + gaps
	}

	actR := 1000
	actC := 1000

	for widx, w := range wndlst {
		r := widx / cols
		c := widx % cols
		if wms.GetWindow(w).Active {
			actR = r
			actC = c
		}
	}

	inst.Log("Grid c:%v*%v+%v r:%v*%v+%v wl:%v", cols, facx, ofsx, rows, facy, ofsy, len(wndlst))
	for widx, w := range wndlst {
		r := widx / cols
		c := widx % cols

		adjx := 0
		adjy := 0
		adjw := 0
		adjh := 0

		if r == actR {
			adjh = tdy
		}
		if r > actR {
			adjy = tdy
		}
		if c == actC {
			adjw = tdx
		}
		if r > actC {
			adjx = tdx
		}

		trl = append(trl, TileResult{ID: w, Geo: Rct{
			X: c*facx + ofsx + adjx,
			Y: r*facy + ofsy + adjy,
			W: usew + adjw,
			H: useh + adjh,
		}})
	}

	inst.TilerHelperProximityAdjust(trl)
	L.Push(inst.LuaHelperTileResultList(L, trl))
	return 1
}

func (inst *Inst) LuaTilerMasterStack(L *lua.LState) int {
	if L.GetTop() == 0 {
		L.Push(lua.LString("MasterStack"))
		L.Push(lua.LString("M"))
		return 2
	}
	wndid := L.CheckString(1)
	scrid := L.CheckString(2)
	wndlst := inst.LuaHelperParseWindowIDList(L, 3)

	wms := inst.WMBE.State(false)
	wnd := wms.GetWindow(wndid)
	scr := wms.GetScreen(scrid)
	if wnd == nil {
		L.Error(lua.LString("invalid window id"), 0)
		return 0
	}
	if scr == nil {
		L.Error(lua.LString("invalid screen id"), 0)
		return 0
	}
	if wndlst == nil {
		L.Error(lua.LString("invalid window list id"), 0)
		return 0
	}

	scrRct, gaps, tdx, tdy := inst.TilerHelperGetScreenLayoutParams(scrid, len(wndlst) == 1)
	_, _ = tdx, tdy

	inst.Log("LayoutParams %v %v %v %v   Gap %v", scrRct.X, scrRct.Y, scrRct.W, scrRct.H, gaps)

	trl := []TileResult{}

	if len(wndlst) == 1 {
		trl = append(trl, TileResult{ID: wndid, Geo: Rct{
			X: scrRct.X,
			Y: scrRct.Y,
			W: scrRct.W,
			H: scrRct.H,
		}})
		L.Push(inst.LuaHelperTileResultList(L, trl))
		return 1
	}

	trl = append(trl, TileResult{ID: wndid, Geo: Rct{
		X: scrRct.X,
		Y: scrRct.Y,
		W: (scrRct.W - gaps) / 2,
		H: scrRct.H,
	}})

	use := scrRct.H
	ofs := scrRct.Y
	fac := scrRct.H
	rows := len(wndlst) - 1
	if len(wndlst) > 2 {
		use = (scrRct.H - gaps*(rows-1)) / rows
		fac = use + gaps
	}
	usedrows := 0
	for _, swnd := range wndlst {
		if swnd != wndid {
			trl = append(trl, TileResult{ID: swnd, Geo: Rct{
				X: scrRct.X + scrRct.W - (scrRct.W-gaps)/2,
				Y: usedrows*fac + ofs,
				W: (scrRct.W - gaps) / 2,
				H: use,
			}})
			usedrows = usedrows + 1
		}
	}
	//	inst.TilerHelperProximityAdjust(trl)

	L.Push(inst.LuaHelperTileResultList(L, trl))
	return 1
}

func (inst *Inst) LuaTilerVTabbed(L *lua.LState) int {
	if L.GetTop() == 0 {
		L.Push(lua.LString("VTabbed"))
		L.Push(lua.LString("V"))
		return 2
	}
	wndid := L.CheckString(1)
	scrid := L.CheckString(2)
	wndlst := inst.LuaHelperParseWindowIDList(L, 3)

	wms := inst.WMBE.State(false)
	wnd := wms.GetWindow(wndid)
	scr := wms.GetScreen(scrid)
	if wnd == nil {
		L.Error(lua.LString("invalid window id"), 0)
		return 0
	}
	if scr == nil {
		L.Error(lua.LString("invalid screen id"), 0)
		return 0
	}
	if wndlst == nil {
		L.Error(lua.LString("invalid window list id"), 0)
		return 0
	}

	scrRct, _, _, _ := inst.TilerHelperGetScreenLayoutParams(scrid, true)

	trl := []TileResult{}

	if len(wndlst) == 1 {
		trl = append(trl, TileResult{ID: wndid, Geo: Rct{
			X: scrRct.X,
			Y: scrRct.Y,
			W: scrRct.W,
			H: scrRct.H,
		}})
		L.Push(inst.LuaHelperTileResultList(L, trl))
		return 1
	}

	// order by activity
	actscore := make(map[WindowID]int)
	for idx, stwndid := range wms.ActiveWindowHistory {
		bonus := 0
		if stwndid == wndid {
			bonus += 10000
		}
		actscore[stwndid] = (idx+1)*1000 + bonus

	}

	sort.Slice(wndlst, func(i, j int) bool {
		s1, _ := actscore[wndlst[i]]
		s2, _ := actscore[wndlst[j]]
		return s1 < s2
	})

	ofsy := 0
	for _, stwndid := range wndlst {
		stwnd := wms.GetWindow(stwndid)
		if stwnd != nil {
			trl = append(trl, TileResult{ID: stwndid, Geo: Rct{
				X: scrRct.X,
				Y: scrRct.Y + ofsy,
				W: scrRct.W,
				H: scrRct.H - ofsy,
			}})
			//todo
			h1 := 20
			h2 := 20
			//			h1 := stwnd.FrameGeometry.H + stwnd.FrameGeometry.Y
			//			h2 := stwnd.Geometry.H - stwnd.InnerGeometry.H
			if h2 > h1 {
				h1 = h2
			}
			ofsy += h2
		}
	}

	L.Push(inst.LuaHelperTileResultList(L, trl))
	return 1
}

func (inst *Inst) LuaTilerVTabbedStagger(L *lua.LState) int {
	if L.GetTop() == 0 {
		L.Push(lua.LString("VTabbedStagger"))
		L.Push(lua.LString("S"))
		return 2
	}
	wndid := L.CheckString(1)
	scrid := L.CheckString(2)
	wndlst := inst.LuaHelperParseWindowIDList(L, 3)

	wms := inst.WMBE.State(false)
	wnd := wms.GetWindow(wndid)
	scr := wms.GetScreen(scrid)
	if wnd == nil {
		L.Error(lua.LString("invalid window id"), 0)
		return 0
	}
	if scr == nil {
		L.Error(lua.LString("invalid screen id"), 0)
		return 0
	}
	if wndlst == nil {
		L.Error(lua.LString("invalid window list id"), 0)
		return 0
	}

	scrRct, gaps, _, _ := inst.TilerHelperGetScreenLayoutParams(scrid, true)

	trl := []TileResult{}

	if len(wndlst) == 1 {
		trl = append(trl, TileResult{ID: wndid, Geo: Rct{
			X: scrRct.X,
			Y: scrRct.Y,
			W: scrRct.W,
			H: scrRct.H,
		}})
		L.Push(inst.LuaHelperTileResultList(L, trl))
		return 1
	}

	// order by activity
	actscore := make(map[WindowID]int)
	for idx, stwndid := range wms.ActiveWindowHistory {
		bonus := 0
		if stwndid == wndid {
			bonus += 10000
		}
		actscore[stwndid] = (idx+1)*1000 + bonus

	}

	sort.Slice(wndlst, func(i, j int) bool {
		s1, _ := actscore[wndlst[i]]
		s2, _ := actscore[wndlst[j]]
		return s1 < s2
	})

	ofsy := 0
	for stidx, stwndid := range wndlst {
		stwnd := wms.GetWindow(stwndid)
		if stwnd != nil {
			trl = append(trl, TileResult{ID: stwndid, Geo: Rct{
				X: scrRct.X + gaps*stidx,
				Y: scrRct.Y + ofsy,
				W: scrRct.W - ofsy*0 - len(wndlst)*gaps,
				H: scrRct.H - ofsy,
			}})
			//todo
			h1 := 20
			h2 := 20
			//			h1 := stwnd.FrameGeometry.H + stwnd.FrameGeometry.Y
			//			h2 := stwnd.Geometry.H - stwnd.InnerGeometry.H
			if h2 > h1 {
				h1 = h2
			}
			ofsy += h2
		}
	}

	L.Push(inst.LuaHelperTileResultList(L, trl))
	return 1
}
