package shinglify

import (
	"math"
)

// TilerHelperProximityAdjust rearranges a tiling result so that windows stay closest to their original position, unless their size changed
func (inst *Inst) TilerHelperProximityAdjust(trl []TileResult) {
	// identify similar-sized groups
	wms := inst.WMBE.State(false)
	trll := len(trl)
	newslot := make([]int, trll)
	slottaken := make([]bool, trll)
	old := make([]Rct, trll)
	for i := 0; i < trll; i++ {
		old[i] = wms.GetWindow(trl[i].ID).Geometry
		newslot[i] = -1
	}

	found := true
	for found {
		found = false
		bestdiff := math.MaxInt
		bestslot := -1
		bestidx := -1
		for i := range trl {
			if newslot[i] == -1 {
				for j := range trl {
					if !slottaken[j] {
						//wd := old[i].W - trl[j].Geo.W
						//hd := old[i].H - trl[j].Geo.H
						/*if wd*wd+hd*hd < 50*/
						{
							xd := old[i].X - trl[j].Geo.X
							yd := old[i].Y - trl[j].Geo.Y
							diff := xd*xd + yd*yd
							if diff < bestdiff {
								bestdiff = diff
								bestslot = j
								bestidx = i
							}
						}
					}
				}
			}
		}
		if bestidx != -1 {
			found = true
			newslot[bestidx] = bestslot
			slottaken[bestslot] = true
		}
	}
	org := append([]TileResult{}, trl...)
	for i := range trl {
		if newslot[i] != -1 {
			trl[i].Geo = org[newslot[i]].Geo
		}
	}
}
