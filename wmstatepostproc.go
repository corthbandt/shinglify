package shinglify

import (
	"fmt"
	"sort"
	"strings"
)

func (inst *Inst) PostProc(wms *WMState) {
	screens := append([]WMScreenState{}, wms.Screens...)
	sort.Slice(screens, func(i, j int) bool {
		return screens[i].ID < screens[j].ID
	})
	scrstr := []string{}
	sclstr := []string{}
	for _, scr := range screens {
		scrstr = append(scrstr, fmt.Sprintf("%v.%v.%v", scr.ID, scr.Product, scr.Serial))
		sclstr = append(sclstr, fmt.Sprintf("%v.%v.%v.%v.%v.%v.%v", scr.ID, scr.Product, scr.Serial, scr.Geometry.X, scr.Geometry.Y, scr.Geometry.W, scr.Geometry.H))
	}
	wms.ScreensConnected = strings.Join(scrstr, ";")
	wms.ScreenLayouts = strings.Join(sclstr, ";")
	for i := range wms.Windows {
		w := wms.Windows[i]
		m := Pnt{X: w.Geometry.X + w.Geometry.W/2, Y: w.Geometry.Y + w.Geometry.H/2}
		for _, scr := range screens {
			if scr.Geometry.Hit(m) {
				w.PrimaryScreen = scr.ID
				if w.Geometry.X >= scr.Geometry.X && w.Geometry.Y >= scr.Geometry.Y && w.Geometry.X+w.Geometry.W <= scr.Geometry.X+scr.Geometry.W && w.Geometry.Y+w.Geometry.H <= scr.Geometry.Y+scr.Geometry.H {
					w.SingleScreen = true
				}
			}
		}

		wms.Windows[i] = w
	}
}
